# httplite
Tiny HTTP server for serving static web pages.

## Configuration
Change the elements in the `resources` array in `config.h`.
You can also modify the `INITIAL_CONN_COUNT` and `PORT` macros.

## License
GPLv3
