/*
    Copyright 2021 Zsolt Vadasz
  
    This file is part of httplite.

    httplite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    httplite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with httplite.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONNECTION_H
#define CONNECTION_H

#include <stdint.h>
#include <poll.h>

/* sets the events field of a pfd */
void init_pfd(struct pollfd *pfd);
/*
 * loops over an array of pfds and
 * calls init_pfd() on them
*/
void init_pfds(struct pollfd *pfds);
/* creates the listener */
void init_listener(int32_t *fd);
/* used for handling incoming connections */
void add_connection(struct pollfd *pfds, int32_t new,
    uint32_t *conn_count, uint32_t *capacity);
/*
 * closes the file descriptor, and removes the
 * pfd at the index i from pfds
*/
void remove_connection(struct pollfd *pfds, uint32_t i,
    uint32_t *conn_count);

#endif /* CONNECTION_H */
