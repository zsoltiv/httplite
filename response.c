/*
    Copyright 2021 Zsolt Vadasz
  
    This file is part of httplite.

    httplite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    httplite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with httplite.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "response.h"


char *response_codes[] =
{
    "200 OK",
    "404 Not Found"
};


char *response_fields[] =
{
    "HTTP/1.1 ",
    "\nDate: ",
    "\nServer: httplite",
    "\nContent-Length: ",
    "\nContent-Type: ",
    "\nAccept-Ranges: bytes",
    "\nConnection: Keep-Alive\r\n\r\n"
};

char *time_str()
{
    time_t rawtime;
    struct tm *info;
    char *buffer;

    buffer = calloc(100, sizeof(char));
    time(&rawtime);
    info = localtime(&rawtime);
    /* store the current time in a formatted string */
    strftime(buffer, 100, "%a, %d %b %Y %X %Z", info);

    /* free unused memory */
    //free(info);

    return buffer;
}

char *get_data_size_str(size_t size)
{
    char *buffer = calloc(22, sizeof(char));
    snprintf(buffer, 22, "%zu", size);
    return buffer;
}

size_t get_header_size(char *code, char *date, const char *content_type, char *data_len_str)
{
    return (
        strlen(response_fields[FIELD_HTTP_VER]) +
        strlen(code) +
        strlen(response_fields[FIELD_DATE]) +
        strlen(date) +
        strlen(response_fields[FIELD_SERVER]) +
        strlen(response_fields[FIELD_CONTENT_TYPE]) +
        strlen(content_type) +
        strlen(response_fields[FIELD_CONTENT_LENGTH]) +
        strlen(data_len_str) +
        strlen(response_fields[FIELD_ACCEPT_RANGES]) +
        strlen(response_fields[FIELD_CONNECTION]));
}

char *build_response(size_t header_size, size_t data_size, char *code, char *date,
    const char *content_type, char *data_len_str, char *data)
{
    char *response = calloc(header_size + data_size, sizeof(char));
    if(!response)
    {
        fprintf(stderr, "out of memory\n");
        exit(1);
    }

    /* copy the strings into response */
    snprintf(response, header_size + 1, "%s%s%s%s%s%s%s%s%s%s%s%s",
        response_fields[FIELD_HTTP_VER],
        code,
        response_fields[FIELD_DATE],
        date,
        response_fields[FIELD_SERVER],
        response_fields[FIELD_CONTENT_LENGTH],
        data_len_str,
        response_fields[FIELD_CONTENT_TYPE],
        content_type,
        response_fields[FIELD_ACCEPT_RANGES],
        response_fields[FIELD_CONNECTION],
        data);
    memcpy(response + header_size, data, data_size);

    fwrite(response, sizeof(char), header_size + data_size, stdout);

    return response;
}
