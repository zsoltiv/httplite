/*
    Copyright 2021 Zsolt Vadasz
  
    This file is part of httplite.

    httplite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    httplite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with httplite.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <netinet/in.h>
#include <poll.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "connection.h"
#include "common.h"

void init_pfd(struct pollfd *pfd)
{
    pfd->events = POLLIN;
}

void init_pfds(struct pollfd *pfds)
{
    for(int32_t i = 0; i < (INITIAL_CONN_COUNT + 1); i++)
    {
        init_pfd(&pfds[i]);
    }
}

void init_listener(int32_t *fd)
{
    struct sockaddr_in serv_addr;
    
    *fd = socket(AF_INET, SOCK_STREAM, 0);
    if((*fd) < 0)
        error("socket()");

    serv_addr.sin_family      =     AF_INET;
    serv_addr.sin_addr.s_addr =  INADDR_ANY;
    serv_addr.sin_port        = htons(PORT);

    int32_t yes = 1;
    setsockopt(*fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int32_t));

    if(bind(*fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("bind()");
    if(listen(*fd, 5) == -1)
    {
        fprintf(stderr, "listen() failed");
        error("listen()");
    }
}

void add_connection(struct pollfd *pfds, int32_t new,
    uint32_t *conn_count, uint32_t *capacity)
{
    if(*conn_count == *capacity)
    {
        /*
         * we need capacity + 1 because
         * the listener is also part of pfds
        */
        *capacity += 1;
        pfds = realloc(pfds, sizeof(struct pollfd) * ((*capacity) + 1));
    }

    *conn_count += 1;

    init_pfd(&pfds[(*conn_count)]);

    pfds[(*conn_count)].fd = new;


    printf("connection\n");
}

void remove_connection(struct pollfd *pfds, uint32_t i,
    uint32_t *conn_count)
{
    close(pfds[i].fd);
    pfds[i] = pfds[(*conn_count)];
    *conn_count -= 1;

    printf("removed connection\n");
}
