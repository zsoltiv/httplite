/*
    Copyright 2021 Zsolt Vadasz
  
    This file is part of httplite.

    httplite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    httplite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with httplite.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef RESOURCE_H
#define RESOURCE_H

#include <stddef.h>

/*
 * allows you to serve different files on
 * different paths like /index.html or /mainpage
*/
struct resource
{
    char *path; /* the url path          */
    char *file; /* path to the file      */
    char *mime; /* MIME type of the file */
};

/**************************************************
 * used for generating resources from files not in*
 * the resources array in config.h                *
**************************************************/

enum mime_index
{
    MIME_WEBP = 0, MIME_JPEG,
    MIME_PNG,      MIME_CSS,
    MIME_HTML,     MIME_MP4,
    MIME_GIF,      MIME_WEBM,
    MIME_FLAC,     MIME_OGG,
    MIME_MP3
};

/* gets the MIME type of a file */
const char *determine_mime_type(const char *file);
/* create a resource */
struct resource *create_resource_from_file(
    const char *file,
    const char *path);
/*
 * compares path to the path of each resource
 * in the array resources (see config.h) and
 * returns the resource with the correct path
 * or NULL if the path doesn't match any of the
 * resources'
*/
struct resource *get_correct_resource(const char *path,
    struct resource *resources, size_t *resource_count);

#endif /* RESOURCE_H */
