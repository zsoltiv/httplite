/*
    Copyright 2021 Zsolt Vadasz
  
    This file is part of httplite.

    httplite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    httplite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with httplite.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef RESPONSE_H
#define RESPONSE_H

#include <string.h>
#include <stdint.h>

enum response_code
{
    CODE_OK = 0,
    CODE_NOT_FOUND
};

enum response_field
{
    FIELD_HTTP_VER = 0,
    FIELD_DATE,
    FIELD_SERVER,
    FIELD_CONTENT_LENGTH,
    FIELD_CONTENT_TYPE,
    FIELD_ACCEPT_RANGES,
    FIELD_CONNECTION
};

enum response_connection
{
    CONN_KEEP_ALIVE = 0,
    CONN_CLOSE
};

extern char *response_codes[];

/* used to get the current time for the response */
char *time_str();
/* returns the response string that will be sent to the client */
char *build_response(size_t header_size, size_t data_size, char *code, char *date,
    const char *content_type, char *data_len_str, char *data);
/* returns the size fo the header excluding the NULL terminatior */
size_t get_header_size(char *code, char *date, const char *content_type, char *data);
/* get the data size as a string */
char *get_data_size_str(size_t size);


#endif /* RESPONSE_H */
