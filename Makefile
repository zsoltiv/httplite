#   Copyright 2021 Zsolt Vadasz
#
#   This file is part of httplite.
#
#   httplite is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   httplite is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with httplite.  If not, see <https://www.gnu.org/licenses/>.

CC=gcc
ERRORS=-Wall -Wno-discarded-qualifiers
CFLAGS=-O2 $(ERRORS) -std=c99

OBJS=httplite.o parser.o response.o resource.o connection.o common.o

default: clean httplite

clean:
	rm -rf ./*.o httplite

httplite: $(OBJS)
	$(CC) $(OBJS) $(CFLAGS) -o httplite

httplite.o: httplite.c
	$(CC) httplite.c $(CFLAGS) -c -o httplite.o
parser.o: parser.c
	$(CC) parser.c $(CFLAGS) -c -o parser.o
response.o: response.c
	$(CC) response.c $(CFLAGS) -c -o response.o
resource.o: resource.c
	$(CC) resource.c $(CFLAGS) -c -o resource.o
connection.o: connection.c
	$(CC) connection.c $(CFLAGS) -c -o connection.o
common.o: common.c
	$(CC) common.c $(CFLAGS) -c -o common.o
