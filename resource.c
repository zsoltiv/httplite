/*
    Copyright 2021 Zsolt Vadasz
  
    This file is part of httplite.

    httplite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    httplite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with httplite.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "resource.h"

static const char *file_extensions[] =
{
    "webp", "jpeg",
    "png", "css",
    "html", "mp4",
    "gif", "webm",
    "flac", "ogg",
    "mp3"
};
static const char *mime_types[] =
{
    "image/webp", "image/jpeg",
    "image/png", "text/css",
    "text/html", "video/mp4",
    "image/gif", "video/webm",
    "audio/flac", "audio/ogg",
    "audio/mpeg"
};

const char *determine_mime_type(const char *file)
{
    size_t str_length = strlen(file);
    char *copy = calloc(str_length + 1, sizeof(char));
    strncpy(copy, file, str_length);
    copy[str_length] = '\0';

    char    *prev;
    char *current;
    char delim[] = ".";

    current = strtok(copy, delim);
    while(current != NULL)
    {
        prev = current;
        current = strtok(NULL, delim);
    }
    if(!prev)
        return NULL;

    if(strcmp(prev, "jpg") == 0)
        prev = "jpeg";

    printf("EXTENSION: %s\n", prev);


    for(int32_t i = MIME_WEBP; i < MIME_MP3; i++)
    {
        if(strcmp(prev, file_extensions[i]) == 0)
            return mime_types[i];
    }

    free(copy);

    return NULL;
}

struct resource *create_resource_from_file(
    const char *file,
    const char *path)
{
    struct resource *res = calloc(1, sizeof(struct resource));
    res->file = file;
    res->mime = determine_mime_type(file);
    res->path = path;

    return res;
}

struct resource *get_correct_resource(const char *path,
    struct resource *resources, size_t *resource_count)
{
    for(int32_t i = 0; i < *resource_count; i++)
    {
        if(strcmp(resources[i].path, path) == 0)
            return &resources[i];
    }

    return NULL;
}
