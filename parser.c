/*
    Copyright 2021 Zsolt Vadasz
  
    This file is part of httplite.

    httplite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    httplite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with httplite.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <stdint.h>

#include "parser.h"

const char *get_path(char *req)
{
    char       *ptr;

    ptr = strtok(req,  " ");
    ptr = strtok(NULL, " ");

    return ptr;
}

const char *get_file_from_path(const char *path)
{
    if(path) {
        if(strcmp(path, "/") == 0)
            return "index.html";
        else
            // omit the preceding '/'
            return &path[1];
    }
    return NULL;
}
