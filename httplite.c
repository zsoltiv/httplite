/*
    Copyright 2021 Zsolt Vadasz
  
    This file is part of httplite.

    httplite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    httplite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with httplite.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#include <unistd.h>
#include <netinet/in.h>
#include <poll.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "config.h"
#include "parser.h"
#include "response.h"
#include "resource.h"
#include "common.h"
#include "connection.h"


/* listener + connections */
struct pollfd *pfds;
/* number of connections */
uint32_t conn_count;
/* max number of connections */
uint32_t capacity;
/* all files to be served go in www/ */
const char *wwwdir = "www/";


/*
 * checks if the capacity is bigger than it needs to be,
 * and resizes pfds
*/
static void free_unused_memory(struct pollfd *pfds,
    uint32_t *conn_count, uint32_t *capacity);
/*
 * loads the file field of
 * a resource and sends it to the client
*/
static void send_resource(int32_t *fd, struct resource *res);


int32_t main(int32_t argc, char **argv)
{
    pfds = calloc((INITIAL_CONN_COUNT + 1), sizeof(struct pollfd));
    if(!pfds)
        error("calloc()");

    init_pfds(pfds);

    init_listener(&pfds[0].fd);

    conn_count = 0;
    capacity = INITIAL_CONN_COUNT;

    while(1)
    {
        int32_t poll_count = poll(pfds, (conn_count + 1), (TIMEOUT_SEC * 1000));
        if(poll_count == -1)
            error("poll()");


        /* main loop */
        if(poll_count > 0)
        {
            for(int32_t i = 0; i <= conn_count; i++)
            {
                printf("current pfd: %d\n", i);
                if(pfds[i].revents & POLLIN)
                {
                    /*
                    * check if the listener is
                    * ready to be read from
                    */
                    if(i == 0)
                    {
                        /* accept incoming connection */
                        struct sockaddr_in cli_addr;

                        socklen_t addrlen = sizeof(cli_addr);
                        int32_t client = accept(pfds[0].fd,
                            (struct sockaddr *) &cli_addr,
                            &addrlen);
                        if(client == -1)
                            error("accept()");
                        add_connection(pfds, client, &conn_count, &capacity);
                        printf("new connection\n");
                    }
                    else
                    {
                        /* regular client */
                        char buffer[4096];

                        int32_t received = recv(pfds[i].fd, buffer, sizeof(buffer), 0);
                        if(received <= 0)
                        {
                            if(received == 0)
                                printf("socket %d disconnected\n", pfds[i].fd);
                            else
                                error("recv()");

                            remove_connection(pfds, i, &conn_count);
                        }
                        else
                        {
                            /* send response */
                            printf("REQUEST:\n%s\n\n", buffer);

                            const char *path = get_path(buffer);

                            struct resource *res = create_resource_from_file(get_file_from_path(path),
                                                                             path);
                            if(!res)
                                fprintf(stderr, "invalid resource\n");
                            else
                                send_resource(&pfds[i].fd, res);

                        }
                    } 
                }
                else if(pfds[i].revents & POLLERR)
                {
                    error("poll()");
                }
            }

            free_unused_memory(pfds, &conn_count, &capacity);
        }
    }

    return 0;
}

static void free_unused_memory(struct pollfd *pfds,
    uint32_t *conn_count, uint32_t *capacity)
{
    if(*capacity >= (INITIAL_CONN_COUNT + 5))
    {
        if(((*capacity) - (*conn_count)) >= 5)
        {
            /* free unused memory */
            *capacity -= 5;
            pfds = realloc(pfds, sizeof(struct pollfd) * ((*capacity) + 1));
        }
    }
}

static void send_resource(int32_t *fd, struct resource *res)
{
    size_t fullpathlen = ((strlen(wwwdir) + strlen(res->file)));
    char *fullpath = calloc(fullpathlen, sizeof(char));

    strcat(fullpath, wwwdir);
    strcat(fullpath, res->file);

    FILE *fp = fopen(fullpath, "rb");
    if(!fp)
    {
        fprintf(stderr, "file: www/%s doesn't exist\n", res->file);
        return;
    }
    
    fseek(fp, 0, SEEK_END);

    size_t data_length = ftell(fp);
    assert(data_length > 0);

    char *buffer = calloc(data_length, sizeof(char));

    fseek(fp, 0, SEEK_SET);

    size_t read = fread(buffer, sizeof(char), data_length, fp);
    if(read < data_length)
        fprintf(stderr, "didn't read the whole file\n");

    char *date = time_str();
    char *data_length_str = get_data_size_str(data_length);

    size_t header_length = get_header_size(response_codes[CODE_OK],
        date, res->mime, data_length_str);
    char *response = build_response(header_length,
        data_length, response_codes[CODE_OK],
        date, res->mime, data_length_str, buffer);

    if(send(*fd, response, header_length + data_length, 0) == -1)
        perror("send()");

    printf("HEADER: :%zu\nDATA :%zuMIME TYPE: %s\n", header_length, data_length, determine_mime_type(fullpath));

    free(date);
    free(data_length_str);
    free(response);
    free(buffer);
    fclose(fp);
}
